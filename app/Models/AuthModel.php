<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class AuthModel extends Model
{
    protected $table = 'm_login';
    protected $primaryKey = 'email';
    protected $allowedFields = ['email','password','fullname','token'];
}