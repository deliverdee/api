<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class BlogsModel extends Model
{
    protected $table = 'trans_blog';
    protected $primaryKey = 'id';
    protected $allowedFields = ['title','author','article'];
}