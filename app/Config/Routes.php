<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// // route since we don't have to scan directories.
// $routes->get('/', 'Home::index');
// $routes->post('auth/create',            'Auth::create');
$routes->resource('auth');
$routes->resource('blogs');

$routes->post('custom/follow','custom::follow');
$routes->post('custom/check','custom::check');
$routes->post('custom/follower','custom::follower');
$routes->post('custom/mutuals','custom::mutuals');
$routes->post('custom/block','custom::block');
$routes->post('custom/readblog','custom::readblog');
$routes->post('custom/checkblog','custom::checkblog');
$routes->post('custom/checkfolloweread','custom::checkfolloweread');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
