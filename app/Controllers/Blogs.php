<?php namespace App\Controllers;
 
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\BlogsModel;
 
class Blogs extends ResourceController
{
    use ResponseTrait;
    // get all product
    public function index()
    {
        $model = new BlogsModel();
        $data = $model->findAll();
        return $this->respond($data, 200);
    }
 
    // get single product
    public function show($id = null)
    {
        $model = new BlogsModel();
        $data = $model->getWhere(['id' => $id])->getResult();
        if($data){
            return $this->respond($data);
        }else{
            return $this->failNotFound('No Data Found with id '.$id);
        }
    }
 
    // create a blog
    public function create() {
        $model = new BlogsModel();
        $data = [
            'title' => $this->request->getVar('title'),
            'article' => $this->request->getVar('article'),
            'author' => $this->request->getVar('author')
        ];
        $model->insert($data);
        $response = [
          'status'   => 201,
          'error'    => null,
          'messages' => [
              'success' => 'Blog post created successfully'
          ]
      ];
      return $this->respondCreated($response);
    }
 
   
 
    // delete product
    public function delete($id = null)
    {
        $model = new BlogsModel();
        $data = $model->find($id);
        if($data){
            $model->delete($id);
            $response = [
                'status'   => 200,
                'error'    => null,
                'messages' => [
                    'success' => 'Data Deleted'
                ]
            ];
             
            return $this->respondDeleted($response);
        }else{
            return $this->failNotFound('No Data Found with id '.$id);
        }
         
    }
 
}