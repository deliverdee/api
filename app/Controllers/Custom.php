<?php 
 
namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;


class Custom extends ResourceController
{
    use ResponseTrait;

    public function follow(){
        $db = \Config\Database::connect();
        $userid=$this->request->getVar('userid');
        $follower=$this->request->getVar('follower');

        $sql = "INSERT INTO trans_follower (userid, follower) VALUES (".$db->escape($userid).", ".$db->escape($follower).")";
        $db->query($sql);
        $data= $db->affectedRows();
        
        return $this->respond($data);
    }

    public function unfollow(){
        $db = \Config\Database::connect();
        $userid=$this->request->getVar('userid');
        $follower=$this->request->getVar('follower');

        $sql = "DELETE FROM trans_follower WHERE userid=".$db->escape($userid)." and follower=".$db->escape($follower).";";
        $db->query($sql);
        $data= $db->affectedRows();
        
        return $this->respond($data);
    }

    public function check(){  
        $db = \Config\Database::connect();
        $userid=$this->request->getVar('userid');
        $follower=$this->request->getVar('follower');
        $sql = "SELECT count(1) as count from trans_follower where userid=".$db->escape($userid)." and follower=".$db->escape($follower)." ";
        $data=$db->query($sql)->getResult();
        return $this->respond($data);
    }

    public function follower(){  
        $db = \Config\Database::connect();
        $userid=$this->request->getVar('userid');
        $sql = "SELECT follower from trans_follower where userid=".$db->escape($userid).";";
        $no=0;
        $followers=array();

        foreach ($db->query($sql)->getResult('array') as $row) {
            array_push($followers,$row['follower']);
            $no++;

        }

        $response = [
            'success' => true,
            'followers' => $followers,
            'count'=> $no,
        ];

        return $this->respond($response);
    }

    public function mutuals(){
        $db = \Config\Database::connect();
        $userida=$this->request->getVar('userida');
        $useridb=$this->request->getVar('useridb');
        $followersa=array();
        $followersb=array();
        $sql = "SELECT follower from trans_follower where userid=".$db->escape($userida).";";

        foreach ($db->query($sql)->getResult('array') as $row) {
            array_push($followersa,$row['follower']);
           

        }

        $sql2 = "SELECT follower from trans_follower where userid=".$db->escape($useridb).";";

        foreach ($db->query($sql2)->getResult('array') as $row2) {
            array_push($followersb,$row2['follower']);
           

        }

        $result=array_intersect($followersa,$followersb);
        $count=sizeof($result);

        $response = [
            'success' => true,
            'followers' => $result,
            'count'=> $count,
        ];

        return $this->respond($response);

    }

    public function block(){
        $db = \Config\Database::connect();
        $userid=$this->request->getVar('userid');
        $block=$this->request->getVar('block');
        $sql = "INSERT INTO trans_block (userid, `block`) VALUES (".$db->escape($userid).", ".$db->escape($block).")";
        $db->query($sql);
        $data= $db->affectedRows();
        $response = [
            'success' => true
        ];

        return $this->respond($response);

    }

    public function readblog(){
        $db = \Config\Database::connect();
        $userid=$this->request->getVar('userid');
        $blogid=$this->request->getVar('blogid');
        $sql = "INSERT INTO trans_blog_read (blogid, `userid`) VALUES (".$db->escape($blogid).", ".$db->escape($userid).")";
        $db->query($sql);
        $data= $db->affectedRows();
        $response = [
            'success' => true
        ];
        return $this->respond($response);
    }

    public function checkblog(){
        $db = \Config\Database::connect();
        $userid=$this->request->getVar('userid');
        $blogid=$this->request->getVar('blogid');
        $sql = "SELECT blogid, `userid` from trans_blog_read where blogid=".$db->escape($blogid)." and `userid`=".$db->escape($userid)." ";
        $db->query($sql);
        $data= $db->affectedRows();
        return $this->respond($data);
    }

    public function checkfolloweread(){
        $db = \Config\Database::connect();
        $userid=$this->request->getVar('userid');
        $author=$this->request->getVar('author');
        $data=array();
        $data2=array();
        $sql="SELECT a.title as title,a.id as id from trans_blog a, trans_blog_read b where a.id=b.blogid and a.author=".$db->escape($author)." and b.userid=".$db->escape($userid)." ;";
        foreach ($db->query($sql)->getResult('array') as $row) {
            $id=$row['id'];
            $title=$row['title'];
            $data[$id]=$title;
        }

        $sql2="SELECT title,id from trans_blog  where author=".$db->escape($author).";";
        foreach ($db->query($sql2)->getResult('array') as $row2) {
            $id=$row2['id'];
            $title=$row2['title'];
            $data2[$id]=$title;
        }

        $result=array_diff($data2,$data);
        $count=sizeof($result);

        $response = [
            'success' => true,
            'blogs' => array_values($result),
            'count'=> $count,
        ];

        return $this->respond($response);
    }



}