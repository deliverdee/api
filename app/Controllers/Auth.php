<?php namespace App\Controllers;
 
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\AuthModel;
 
class Auth extends ResourceController
{
    use ResponseTrait;
    // get all product
    public function index()
    {
        $model = new AuthModel();
        $data = $model->findAll();
        return $this->respond($data, 200);
    }
 
    // get single product
    public function show($id = null)
    {
        $model = new AuthModel();
        $data = $model->getWhere(['email' => $id])->getResult();
        if($data){
            return $this->respond($data);
        }else{
            return $this->failNotFound('No Data Found with email '.$id);
        }
    }


    public function create() {
        $model = new AuthModel();
        $data = [
            'fullname' => $this->request->getVar('fullname'),
            'email' => $this->request->getVar('email'),
            'password' => $this->request->getVar('password')
        ];
        $model->insert($data);
        $response = [
          'status'   => 201,
          'error'    => null,
          'messages' => [
              'success' => 'Employee created successfully'
          ]
      ];
      return $this->respondCreated($response);
    }
 
   
 
    // delete auth
    public function delete($id = null)
    {
        $model = new AuthModel();
        $data = $model->find($id);
        if($data){
            $model->delete($id);
            $response = [
                'status'   => 200,
                'error'    => null,
                'messages' => [
                    'success' => 'Data Deleted'
                ]
            ];
             
            return $this->respondDeleted($response);
        }else{
            return $this->failNotFound('No Data Found with id '.$id);
        }
         
    }
 
}